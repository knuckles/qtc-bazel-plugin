#pragma once

namespace BazelProjectManager::Internal::Constants {

namespace Project {
const char MIMETYPE[] = "text/x-bazel";
const char ID[]       = "BazelProjectManager.BazelProject";
}  // namespace Project

namespace Icons {
const char BUILD_ICON[]           = ":/projectexplorer/images/build.png";
const char PACKAGE_OVERLAY_ICON[] = ":/bazelprojectmanager/images/bazel-overlay-icon.png";
const char BAZEL_ICON[]           = ":/bazelprojectmanager/images/bazel-icon.png";
}  // namespace Icons

}  // namespace BazelProjectManager::Internal::Constants
